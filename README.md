# Vodaphone Bowling Score Test #

The code to solve the Bowling Score Test exercise can be built and executed in a Maven command

~~~
mvn clean install cobertura:cobertura
~~~

The maven script executes the specified core tests, plus a wide variety of additional tests.

### Design Notes ###

The entry point to the solution is in the class BowlingScore. No parameters are taken from the arguments to main as all input and utput is via System.out and System.in. This was chosen to allow standard Unix pipe coding.

Two packages then hold the domain model and the parser to read the input string.

### Execution Notes ###

A pretty wide variety of tests is executed through the mvn command above, and the code can be explored through the generated Cobertura report (in directory ./target/site/)

The repository includes build configuration for the Eclipse IDE, but the Maven script is easily executable from any IDE or the command line.
