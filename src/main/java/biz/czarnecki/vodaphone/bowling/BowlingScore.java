package biz.czarnecki.vodaphone.bowling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import biz.czarnecki.vodaphone.bowling.domain.Game;
import biz.czarnecki.vodaphone.bowling.parser.ScoreParser;

public class BowlingScore {

	private InputStream in;
	
	private PrintStream out;	
	
	public static void main(String[] args) {
		BowlingScore bowlingScore = new BowlingScore(System.in, System.out);
		bowlingScore.score();
	}

	public BowlingScore(InputStream in, PrintStream out) {
		this.in = in;
		this.out = out;
	}

	public void score () {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String s;

		try {
			while ((s = reader.readLine()) != null && s.length() != 0) {
				Game game = new Game(ScoreParser.parse(s));
				out.println(game.score());
			}
		} 
		catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

}
