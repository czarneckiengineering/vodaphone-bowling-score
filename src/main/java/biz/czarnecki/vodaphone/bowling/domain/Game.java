package biz.czarnecki.vodaphone.bowling.domain;

import java.util.ArrayList;

public class Game implements Scoreable {
	
	private ArrayList<Ball> balls = new ArrayList<Ball>();

	public Game (ArrayList<Ball> balls) {
		this.balls = balls;
	}

	public int score() {
		
		ArrayList<Frame> frames = buildFrames();
		
		int score = 0;
		
		for (Frame frame : frames) {
			score += frame.score();
		}
		
		return score;
	}

	private ArrayList<Frame> buildFrames() {
		ArrayList<Frame> frames = new ArrayList<Frame>();
		
		// build 'frames' until we run out of data
		for (int gameBallIndex = 0; gameBallIndex < balls.size(); gameBallIndex++) {
			ArrayList<Ball> ballsForFrame = new ArrayList<Ball>();

			// each frame always has three balls that might count to the frame score until the 1th / 12th frame
			for (int frameBallIndex = gameBallIndex; frameBallIndex < balls.size() && ballsForFrame.size() < 3; frameBallIndex++) {
				ballsForFrame.add(balls.get(frameBallIndex));
			}

			if (frames.size() < 10) {
				frames.add(new Frame(ballsForFrame));
			}

			// if the first ball in a frame does NOT score 10 then the next frame starts in TWO balls time
			if (ballsForFrame.get(0).score() != 10) {
				gameBallIndex++;
			}
		}
		
		return frames;
	}

	@Override
	public String toString() {
		return "Game [balls=" + balls + "]";
	}
	
}
