package biz.czarnecki.vodaphone.bowling.domain;

public class Ball implements Scoreable {
	
	private int value;

	public int score() {
		return value;
	}
	
	private Ball (int value) {
		this.value = value;
	}
	
	public static Ball parse (String s) throws NumberFormatException {
		int value = Integer.parseInt(s);
		
		if (value < 0)
			throw new NumberFormatException("Negative scores are invalid");
		
		if (value > 10)
			throw new NumberFormatException("Scores greater than 11 are invalid");

		return new Ball (value);
	}

	@Override
	public String toString() {
		return "Ball [value=" + value + "]";
	}

}
