package biz.czarnecki.vodaphone.bowling.domain;

public interface Scoreable {
	
	int score ();

}
