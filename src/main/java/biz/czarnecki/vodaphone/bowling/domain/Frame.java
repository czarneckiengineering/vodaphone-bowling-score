package biz.czarnecki.vodaphone.bowling.domain;

import java.util.ArrayList;

public class Frame implements Scoreable {

	private ArrayList<Ball> balls = new ArrayList<Ball>();

	public int score() {
		int score = 0;
		
		for (int i = 0; i <= balls.size() - 1; i++) {
			Ball ball = balls.get(i);
			
			score += ball.score();
			
			if (i == 1 && score < 10) {
				break;
			}
		}
		
		return score;
	}

	public Frame (ArrayList<Ball> balls) {
		this.balls = balls;
	}

	@Override
	public String toString() {
		return "Frame [balls=" + balls + "]";
	}
}
