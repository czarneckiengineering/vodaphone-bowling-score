package biz.czarnecki.vodaphone.bowling.parser;

import java.util.ArrayList;

import biz.czarnecki.vodaphone.bowling.domain.Ball;

public class ScoreParser {
	
	public static ArrayList<Ball> parse(String s) {
		ArrayList<Ball> balls = new ArrayList<Ball>();
		
		String[] lexemes = s.trim().split("[\\s]+");
		
		for (int i = 0; i < lexemes.length; i++) {
			balls.add(Ball.parse(lexemes[i]));
		}
		
		return balls;
	}
	
}
