package biz.czarnecki.vodaphone.bowling.parser;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import biz.czarnecki.vodaphone.bowling.domain.Ball;

public class ScoreParserTest {

	@Test
	public void testParseFourNumbers() {
		String input = "1 2 3 4";
		
		ArrayList<Ball> balls = ScoreParser.parse(input);
		
		assertEquals(4, balls.size());
	}

	@Test
	public void testParseFourNumbersExtraWhitespace() {
		String input = "           1   2 3 \t 4    ";
		
		ArrayList<Ball> balls = ScoreParser.parse(input);
		
		assertEquals(4, balls.size());
	}

}
