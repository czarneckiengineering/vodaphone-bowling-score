package biz.czarnecki.vodaphone.bowling.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class GameTest {

	@Test
	public void testScore() {
		
		Ball ball1 = Ball.parse("1");
		Ball ball2 = Ball.parse("1");
		Ball ball3 = Ball.parse("1");
		Ball ball4 = Ball.parse("1");
		
		ArrayList<Ball> balls = new ArrayList<Ball>();
		
		balls.add(ball1);
		balls.add(ball2);
		balls.add(ball3);
		balls.add(ball4);
		
		Game game = new Game (balls);
		
		assertEquals(4, game.score());
	}

}
