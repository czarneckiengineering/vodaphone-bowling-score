package biz.czarnecki.vodaphone.bowling.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class BallTest {

	@Test
	public void testScore() {
		Ball ball = Ball.parse("5");
		assertEquals(5, ball.score());
	}

	@Test
	public void testParseScoreZero() {
		
		Ball ball = Ball.parse("0");
		
		assertEquals(ball.score(), 0);
	}

	@Test
	public void testParseScoreOne() {
		
		Ball ball = Ball.parse("1");
		
		assertEquals(ball.score(), 1);
	}

	@Test(expected = NumberFormatException.class)
	public void testParseScoreEleven() {
		
		Ball ball = Ball.parse("11");
		
		assertEquals(ball.score(), 11);
	}

	@Test(expected = NumberFormatException.class)
	public void testParseScoreNegativeONe() {
		
		Ball ball = Ball.parse("-1");
		
		assertEquals(ball.score(), -1);
	}

}
