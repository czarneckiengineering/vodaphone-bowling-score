package biz.czarnecki.vodaphone.bowling.domain;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

public class FrameTest {

	@Test
	public void testScoreTwoBallsNoStrikeNoSpare() {
		Ball ball1 = Ball.parse("1");
		Ball ball2 = Ball.parse("1");
		
		ArrayList<Ball> balls = new ArrayList<Ball> ();
		balls.add(ball1);
		balls.add(ball2);
		
		Frame frame = new Frame (balls);
		
		assertEquals(2, frame.score());
	}

	@Test
	public void testScoreThreeBallsNoStrikeNoSpare() {
		Ball ball1 = Ball.parse("1");
		Ball ball2 = Ball.parse("1");
		Ball ball3 = Ball.parse("1");
		
		ArrayList<Ball> balls = new ArrayList<Ball> ();
		balls.add(ball1);
		balls.add(ball2);
		balls.add(ball3);
		
		Frame frame = new Frame (balls);
		
		assertEquals(2, frame.score());
	}

	@Test
	public void testScoreFourBallsNoStrikeNoSpare() {
		Ball ball1 = Ball.parse("1");
		Ball ball2 = Ball.parse("1");
		Ball ball3 = Ball.parse("1");
		Ball ball4 = Ball.parse("1");
		
		ArrayList<Ball> balls = new ArrayList<Ball> ();
		balls.add(ball1);
		balls.add(ball2);
		balls.add(ball3);
		balls.add(ball4);
		
		Frame frame = new Frame (balls);
		
		assertEquals(2, frame.score());
	}

	@Test
	public void testScoreThreeBallsNoStrikeOneSpare() {
		Ball ball1 = Ball.parse("9");
		Ball ball2 = Ball.parse("1");
		Ball ball3 = Ball.parse("1");
		
		ArrayList<Ball> balls = new ArrayList<Ball> ();
		balls.add(ball1);
		balls.add(ball2);
		balls.add(ball3);
		
		Frame frame = new Frame (balls);
		
		assertEquals(11, frame.score());
	}

	@Test
	public void testScoreThreeBallsOneStrikeNoSpare() {
		Ball ball1 = Ball.parse("10");
		Ball ball2 = Ball.parse("1");
		Ball ball3 = Ball.parse("1");
		
		ArrayList<Ball> balls = new ArrayList<Ball> ();
		balls.add(ball1);
		balls.add(ball2);
		balls.add(ball3);
		
		Frame frame = new Frame (balls);
		
		assertEquals(12, frame.score());
	}
	
	@Test
	public void testScoreThreeBallsOneStrikeOneSpare() {
		Ball ball1 = Ball.parse("10");
		Ball ball2 = Ball.parse("9");
		Ball ball3 = Ball.parse("1");
		
		ArrayList<Ball> balls = new ArrayList<Ball> ();
		balls.add(ball1);
		balls.add(ball2);
		balls.add(ball3);
		
		Frame frame = new Frame (balls);
		
		assertEquals(20, frame.score());
	}

	@Test
	public void testScoreThreeBallsTwoStrikesNoSpares() {
		Ball ball1 = Ball.parse("10");
		Ball ball2 = Ball.parse("10");
		Ball ball3 = Ball.parse("1");
		
		ArrayList<Ball> balls = new ArrayList<Ball> ();
		balls.add(ball1);
		balls.add(ball2);
		balls.add(ball3);
		
		Frame frame = new Frame (balls);
		
		assertEquals(21, frame.score());
	}

}
