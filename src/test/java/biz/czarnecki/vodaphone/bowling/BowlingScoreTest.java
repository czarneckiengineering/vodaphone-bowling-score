package biz.czarnecki.vodaphone.bowling;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class BowlingScoreTest {
	
	@Test
	public void testScoreFourBallsTwoFramesNoSparesNoStrikes() {
		String input = "1 2 3 4";
		
		String output = scoreInput (input);
		
		assertEquals("10", output);
	}
	
	@Test
	public void testScoreTwelveBallsTenFramesNoSparesTwelveStrikes() {
		String input = "10 10 10 10 10 10 10 10 10 10 10 10";
		
		String output = scoreInput (input);
		
		assertEquals("300", output);
	}
	
	@Test
	public void testScoreTwentyBallsTenFramesNoSparesTwelveStrikes() {
		String input = "10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10";
		
		String output = scoreInput (input);
		
		assertEquals("300", output);
	}
	
	@Test
	public void testScoreFourBallsTwoFramesTwoSparesNoStrikes() {
		String input = "9 1 9 1";
		
		String output = scoreInput (input);
		
		assertEquals("29", output);
	}
	
	@Test
	public void testScoreSevenBallsFourFramesTwoSparesNoStrikes() {
		String input = "1 1 1 1 10 1 1";
		
		String output = scoreInput (input);
		
		assertEquals("18", output);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testScoreOneBallsTooBigNumber() {
		String input = "11";
		
		String output = scoreInput (input);
		
		assertEquals("11", output);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testScoreOneBallsNegativeNumber() {
		String input = "-1";
		
		String output = scoreInput (input);
		
		assertEquals("-1", output);
	}
	
	@Test(expected = NumberFormatException.class)
	public void testScoreOneBallsNotANumber() {
		String input = "Hello";
		
		String output = scoreInput (input);
		
		assertEquals("Hello", output);
	}
	
	
	
	private String scoreInput (String input) {
		InputStream in = new java.io.ByteArrayInputStream(input.getBytes());
		
		OutputStream output = new java.io.ByteArrayOutputStream();
		PrintStream out = new PrintStream(output);

		BowlingScore bowlingScore = new BowlingScore(in, out);
		
		bowlingScore.score();
		
		return output.toString().trim();
	}

}
